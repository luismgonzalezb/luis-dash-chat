const shortid = require('shortid');

// Simple in memory database
const database = [];
let users = [];

// Utility functions
const findRoom = (roomId) => {
  const room = database.find(rm => rm.id === roomId);
  if (room === undefined) {
    return { error: `a room with id ${roomId} does not exist` };
  }
  return room;
};

const findRoomByName = (name) => {
  const room = database.find(rm => rm.name === name);
  if (room === undefined) {
    return { error: `a room with name ${name} does not exist` };
  }
  return room;
};

const findMessage = (room, messageId) => {
  const message = room.messages.find(msg => msg.id === messageId);
  if (room === undefined) {
    return { error: `a message with id ${messageId} does not exist` };
  }
  return message;
};

const userExists = (name) => {
  const user = users.find(usr => usr === name);
  if (user === undefined) {
    return false;
  }
  return true;
};

const logUser = (room, username) => {
  const userNotLogged = !room.users.find(user => user === username);
  if (userNotLogged) {
    room.users.push(username);
  }
};

const getRooms = () =>
  database.map(room => ({ name: room.name, id: room.id }));

const getRoom = (id) => {
  const room = findRoom(id);
  if (room.error) {
    return room;
  }
  return { name: room.name, id: room.id, users: room.users };
};

const postRoom = (name) => {
  const room = findRoomByName(name);
  if (room.error) {
    const newRoom = {
      name,
      id: shortid.generate(),
      messages: [],
      users: [],
    };
    database.push(newRoom);
    return newRoom;
  }
  return { error: 'Room already exists' };
};

const login = (name) => {
  if (userExists(name)) {
    return { error: 'User Already Exists' };
  }
  users.push(name);
  return { success: true };
};

const logout = (name) => {
  if (userExists(name)) {
    users = users.filter(usr => usr !== name);
    return { success: true };
  }
  return { error: 'User Does Not Exists' };
};

const getMessages = (id) => {
  const room = findRoom(id);
  if (room.error) {
    return room;
  }
  return room.messages;
};

const postMessage = (id, name, message) => {
  const room = findRoom(id);
  if (room.error) {
    return room;
  }
  if (!name) {
    return { error: 'User Name Missing' };
  }
  if (!message) {
    return { error: 'User Name Missing' };
  }
  logUser(room, name);
  const newMessage = {
    name,
    message,
    id: shortid.generate(),
    time: new Date(),
    reactions: [],
  };
  room.messages.push(newMessage);
  return newMessage;
};

const reactToMessage = (roomId, messageId, name, reaction) => {
  const room = findRoom(roomId);
  if (room.error) {
    return room;
  }
  const message = findMessage(room, messageId);
  if (message.error) {
    return message;
  }
  message.reactions.push({ name, reaction });
  return message;
};

module.exports = {
  data: database,
  users,
  login,
  logout,
  getRooms,
  getRoom,
  postRoom,
  getMessages,
  postMessage,
  reactToMessage,
};
