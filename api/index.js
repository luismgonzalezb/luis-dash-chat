const actions = require('../src/data/constants/actions');
const database = require('./database');

const onErrorAction = err => ({ type: actions.SERVER_ERROR, err });

// All the actions are implemented on the same concept,
// It returns a promise (this in case the database actions are async, instead of the current array implementation)
// Once the "databse" action is executed, we resolve the promise with error or success.
// The data passed into the resolition is then emmited for the client to listen.
const login = action =>
  new Promise((resolve, reject) => {
    const user = database.login(action.userName);
    if (user.error) {
      return reject(user.error);
    }
    return resolve({
      type: actions.LOG_IN_SUCESS,
      user: {
        name: action.userName,
        time: new Date(),
      },
    });
  });

const logout = action =>
  new Promise((resolve, reject) => {
    const user = database.logout(action.userName);
    if (user.error) {
      return reject(user.error);
    }
    return resolve({
      type: actions.LOG_OUT_SUCESS,
    });
  });

const getRooms = () =>
  new Promise((resolve) => {
    const rooms = database.getRooms();
    resolve({ type: actions.GET_ROOMS_COMPLETED, rooms });
  });

const postRoom = action =>
  new Promise((resolve, reject) => {
    const room = database.postRoom(action.name);
    if (room.error) {
      return reject(room.error);
    }
    return resolve({ type: actions.POST_ROOM_COMPLETED, room });
  });

const getRoomDetails = action =>
  new Promise((resolve, reject) => {
    const room = database.getRoom(action.id);
    if (room.error) {
      return reject(room.error);
    }
    return resolve({ type: actions.GET_DETAILS_COMPLETED, room });
  });

const getRoomMessages = action =>
  new Promise((resolve, reject) => {
    const messages = database.getMessages(action.id);
    if (messages.error) {
      return reject(messages.error);
    }
    return resolve({
      type: actions.GET_MESSAGES_COMPLETED,
      room: {
        id: action.id,
        messages,
      },
    });
  });

const postMessage = action =>
  new Promise((resolve, reject) => {
    const message = database.postMessage(action.id, action.name, action.message);
    if (message.error) {
      return reject(message.error);
    }
    return resolve({
      type: actions.POST_MESSAGE_COMPLETED,
      id: action.id,
      message,
    });
  });

const reactToMessage = action =>
  new Promise((resolve, reject) => {
    const message = database.reactToMessage(action.roomId, action.messageId, action.name, action.reaction);
    if (message.error) {
      return reject(message.error);
    }
    return resolve({
      type: actions.REACT_TO_MESSAGE_COMPLETED,
      id: action.roomId,
      message,
    });
  });

// The key mapping for the handler functions is based on the constants that also the client uses
const ACTION_HANDLERS = {
  [actions.LOG_IN]: login,
  [actions.LOG_OUT]: logout,
  [actions.GET_ROOMS]: getRooms,
  [actions.POST_ROOM]: postRoom,
  [actions.GET_DETAILS]: getRoomDetails,
  [actions.GET_MESSAGES]: getRoomMessages,
  [actions.POST_MESSAGE]: postMessage,
  [actions.REACT_TO_MESSAGE]: reactToMessage,
};

module.exports = {
  actions(socket) {
    // Socket is listening for clients to emit an 'action' message
    // These messages are emmited from the client with Redux Help
    socket.on('action', (action) => {
      // Instead of using a switch, we use an object with keys for the actions
      const handler = ACTION_HANDLERS[action.type];
      // If the action type exists in the object, the function to be executed is grabbed
      // and then executed, passinging the action object (from Redux actions)
      if (handler) {
        handler(action)
          .then((data) => {
            // If the action initiated by the user is something other users need to know
            // The result is broadcasted to all clients for real time updates
            if (action.type === actions.POST_MESSAGE
              || action.type === actions.POST_ROOM
              || action.type === actions.REACT_TO_MESSAGE) {
              socket.broadcast.emit('action', data);
            }
            // This emits the result of the action only to the client that initiated it.
            // (See: store.js for more explanation on Redux Implementation of Socket.io)
            return socket.emit('action', data);
          })
          // All errors are handled by the same Redux Reducer and display message to user.
          .catch(err => socket.emit('action', onErrorAction(err)));
      }
    });
  },
};
