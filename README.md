# Instructions / Comments by Luis

## Getting Started

I used Node v8.4.0 and Yarn v0.27.5 (you can use npm the same), I used react-create-app as a base and modified the scripts to integrate with api server.

### 1. To simply start the app for testing run

```
yarn start
```

You can access the app at:

```
http://localhost:8080
```

### 2. To develop run, this watches both src and api folders.

```
yarn dev & yarn api-dev
```

You can access the app at:

```
http://localhost:3000
```

### 3. To run some simple test, it runs the create-react-app base test and I added linting with the Airbnb Style Guide.

```
yarn test
```

## General Idea, Tech & Architecture

I used React for this project, I've been working on it for a while now, for state management I used Redux, in this instace router is not quired since there is a single endpoint.

The most interesting part is the use of Socket.io and the redux-socket.io packages.

Traditionally actions will call an async api (with some fetch implementation), and dispatch the result once it comes. With the use of Socket.io, a well known socket implementation for Node.js,and the addition of redux-socket.io made it easier, the latter has a middleware function that wraps the socket with in a dispatch listener that matches a pattern to route the actions to the server, it also dispatches actions when the server emits events with the same pattern.

I also made some modifications to the api, extracted the database to it's own file and used promises, this would allow to transition to an acutal database with a little more ease.

I left the original api endpoints to simply test the response and db (memory) status. But all communications with the client are made trugh Socket.io

I hope you enjoy the review. Have a nie one :)

## Test online

Finally I put the test app on Heroku, I'll have it up for a few days, then I'll take it down since it's pretty open and unsecure.

```
https://luis-dash-chat.herokuapp.com/
```