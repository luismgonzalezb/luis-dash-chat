import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import cx from 'classnames';
import Reactions from './Reactions';
import './Messages.css';

class Messages extends Component {
  componentDidUpdate() {
    this.messagesContainer.scrollTop = this.messagesContainer.scrollHeight;
  }
  render() {
    const { user, messages, onReaction } = this.props;
    return (
      <div className="messages" ref={(ref) => { this.messagesContainer = ref; }}>
        {messages.map(msg => (
          <div key={msg.id} className="msg">
            <div className={cx('msg-content', { me: msg.name === user.name })}>
              <p>{msg.message}</p>
              <Reactions user={user} message={msg} onReaction={onReaction} />
            </div>
            <div className={cx('msg-details', { me: msg.name === user.name })}>
              By <span>{msg.name}</span> at <span>{moment(msg.time).format('MMM Do YY, h:mm:ss a')}</span>
            </div>
          </div>
        ))}
      </div>
    );
  }
}

Messages.propTypes = {
  user: PropTypes.object.isRequired,
  messages: PropTypes.array.isRequired,
  onReaction: PropTypes.func.isRequired,
};

export default Messages;
