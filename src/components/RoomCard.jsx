import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './RoomCard.css';

const RoomCard = ({ room, selectRoom, selected }) => (
  <li key={room.id} data-id={room.id} className={cx('room-card', { selected })}>
    <a
      className={cx({ selected })}
      href={`#${room.name}`}
      onClick={(e) => {
        e.preventDefault();
        selectRoom(room.id);
      }}
    >
      {room.name}
    </a>
  </li>
);

RoomCard.propTypes = {
  room: PropTypes.object.isRequired,
  selected: PropTypes.bool.isRequired,
  selectRoom: PropTypes.func.isRequired,
};

export default RoomCard;
