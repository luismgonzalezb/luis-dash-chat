import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './FieldForm.css';

class FieldForm extends Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentDidMount() {
    this.input.focus();
  }
  onChange(evt) {
    const value = evt.target.value;
    this.setState({ value });
  }
  onSubmit(evt) {
    const { value } = this.state;
    if (value) {
      this.setState({ value: '' });
      this.props.onSubmit(value);
    }
    evt.preventDefault();
  }
  render() {
    const { id, name, placeholder, btnLabel, className, maxLength } = this.props;
    const { value } = this.state;
    return (
      <form onSubmit={this.onSubmit} className={className}>
        <input
          id={id}
          type="text"
          name={name}
          value={value}
          maxLength={maxLength}
          placeholder={placeholder}
          onChange={this.onChange}
          ref={(ref) => { this.input = ref; }}
        />
        <button type="submit">{btnLabel}</button>
      </form>
    );
  }
}

FieldForm.defaultProps = {
  id: '',
  name: 'text',
  placeholder: 'Enter Value',
  btnLabel: 'Submit',
  className: 'form-row',
  maxLength: 25,
};

FieldForm.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  btnLabel: PropTypes.string,
  className: PropTypes.string,
  maxLength: PropTypes.number,
  onSubmit: PropTypes.func.isRequired,
};

export default FieldForm;
