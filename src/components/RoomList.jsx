import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import FieldForm from '../components/FieldForm';
import RoomCard from '../components/RoomCard';
import './RoomList.css';

const RoomList = ({ user, rooms, selectRoom, postRoom, selectedId }) => (
  <nav className="rooms">
    <div>
      <div className="user-name">{user.name}</div>
      <span className="user-time">Online since {moment(user.time).fromNow()}</span>
      {rooms.length ? (
        <ul className="room-list">
          {rooms.map(room => (
            <RoomCard key={room.id} room={room} selectRoom={selectRoom} selected={selectedId === room.id} />
          ))}
        </ul>
      ) : (
        <h4>There are no rooms !</h4>
      )}
    </div>
    <FieldForm
      name="roomName"
      className="form-col"
      placeholder="Enter room name..."
      btnLabel="Create Room"
      onSubmit={postRoom}
    />
  </nav>
);

RoomList.propTypes = {
  user: PropTypes.object.isRequired,
  rooms: PropTypes.array.isRequired,
  selectedId: PropTypes.string.isRequired,
  selectRoom: PropTypes.func.isRequired,
  postRoom: PropTypes.func.isRequired,
};

export default RoomList;
