import React from 'react';
import PropTypes from 'prop-types';
import './Reactions.css';

const Reactions = ({ message, onReaction }) => {
  const ups = message.reactions.filter(react => react.reaction === 'up').length;
  const donws = message.reactions.filter(react => react.reaction === 'down').length;
  const loves = message.reactions.filter(react => react.reaction === 'love').length;
  return (
    <div className="reactions">
      <button type="button" className="reaction" onClick={() => onReaction(message.id, 'up')}>
        <i className="fa fa-thumbs-o-up" />
        <span>{ups || ''}</span>
      </button>
      <button type="button" className="reaction" onClick={() => onReaction(message.id, 'down')}>
        <i className="fa fa-thumbs-o-down" />
        <span>{donws || ''}</span>
      </button>
      <button type="button" className="reaction" onClick={() => onReaction(message.id, 'love')}>
        <i className="fa fa-heart-o" />
        <span>{loves || ''}</span>
      </button>
    </div>
  );
};

Reactions.propTypes = {
  message: PropTypes.object.isRequired,
  onReaction: PropTypes.func.isRequired,
};

export default Reactions;
