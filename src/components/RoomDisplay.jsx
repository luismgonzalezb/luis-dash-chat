import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Messages from '../components/Messages';
import FieldForm from '../components/FieldForm';
import './RoomDisplay.css';

const RoomDisplay = ({ room, user, onReaction, postMessage }) => {
  if (!room.name) {
    return (<div className="room-select center-middle">
      <span>Please Select a Room!</span>
    </div>);
  }
  return (
    <div className="room-content">
      <header className="room-title">
        <h2>{room.name}</h2>
        <ul className="room-users">
          {room.users && room.users.map(u => (
            <li key={u} className={cx({ me: u === user.name })}>{u}</li>
          ))}
        </ul>
      </header>
      <Messages user={user} messages={room.messages || []} onReaction={onReaction} />
      <footer className="room-input">
        <FieldForm
          id="message"
          name="message"
          btnLabel="Send"
          placeholder="Enter a message..."
          maxLength={500}
          onSubmit={postMessage}
        />
      </footer>
    </div>
  );
};

RoomDisplay.defaultProps = {
  room: {},
};

RoomDisplay.propTypes = {
  room: PropTypes.object,
  user: PropTypes.object.isRequired,
  postMessage: PropTypes.func.isRequired,
  onReaction: PropTypes.func.isRequired,
};

export default RoomDisplay;
