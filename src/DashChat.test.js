/* global it */
/* eslint react/jsx-filename-extension: off */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import DashChat from './DashChat';
import store from './data/store';

const newStore = store();

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <Provider store={newStore}>
      <DashChat />
    </Provider>,
    div);
});
