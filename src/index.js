/** Prevent an error from es lint to keep this js (not jsx) file due to create-react-app config */
/* eslint react/jsx-filename-extension: off */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import DashChat from './DashChat';
import registerServiceWorker from './registerServiceWorker';
import store from './data/store';

import './index.css';

const newStore = store();

ReactDOM.render(
  <Provider store={newStore}>
    <DashChat />
  </Provider>,
  document.getElementById('root'),
);
registerServiceWorker();
