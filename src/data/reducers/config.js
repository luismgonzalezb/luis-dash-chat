import {
  SERVER_ERROR,
} from '../constants/actions';

export const serverErrorReducer = (config, action) => Object.assign({}, config, {
  error: {
    msg: action.err,
    time: new Date(),
  },
});

const ACTION_HANDLERS = {
  [SERVER_ERROR]: serverErrorReducer,
};

export default function reducer(state = {}, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
