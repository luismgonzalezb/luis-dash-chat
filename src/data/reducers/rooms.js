import {
  GET_ROOMS_COMPLETED,
  POST_ROOM_COMPLETED,
  GET_DETAILS_COMPLETED,
  GET_MESSAGES_COMPLETED,
  POST_MESSAGE_COMPLETED,
  REACT_TO_MESSAGE_COMPLETED,
} from '../constants/actions';

const getRoomsReducer = (rooms, action) => action.rooms;

const postRoomReducer = (rooms, action) => [...rooms, action.room];

const getRoomDetailsReducer = (rooms, action) => rooms.map(room => (room.id === action.room.id ? Object.assign({}, room, action.room) : room));

const getRoomMessagesReducer = getRoomDetailsReducer;

const getNewUsers = (currentUsers, newName) => {
  if (!currentUsers) {
    return [newName];
  }
  const found = currentUsers.filter(currentName => currentName === newName)[0];
  if (!found) {
    return [newName, ...currentUsers];
  }
  return currentUsers;
};

const postMessageReducer = (rooms, action) => rooms.map(room => (room.id === action.id ? Object.assign({}, room, {
  messages: room.messages ? [...room.messages, action.message] : [action.message],
  users: getNewUsers(room.users, action.message.name),
}) : room));

const reactToMessageReducer = (rooms, action) => rooms.map(room => (room.id === action.id ? Object.assign({}, room, {
  messages: room.messages ? room.messages.map(msg => (
    msg.id === action.message.id ? Object.assign({}, msg, action.message) : msg
  )) : [action.message],
}) : room));

// All handlers for the Reducers are mapped the same way
// (same on the server) to avoid writing switch statements.
const ACTION_HANDLERS = {
  [GET_ROOMS_COMPLETED]: getRoomsReducer,
  [POST_ROOM_COMPLETED]: postRoomReducer,
  [GET_DETAILS_COMPLETED]: getRoomDetailsReducer,
  [GET_MESSAGES_COMPLETED]: getRoomMessagesReducer,
  [POST_MESSAGE_COMPLETED]: postMessageReducer,
  [REACT_TO_MESSAGE_COMPLETED]: reactToMessageReducer,
};

export default function reducer(state = [], action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
