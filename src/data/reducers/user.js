import {
  LOG_IN_SUCESS,
} from '../constants/actions';

export const loginReducer = (user, action) => action.user;

const ACTION_HANDLERS = {
  [LOG_IN_SUCESS]: loginReducer,
};

export default function reducer(state = {}, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
