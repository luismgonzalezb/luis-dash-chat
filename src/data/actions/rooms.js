import {
  GET_ROOMS,
  POST_ROOM,
  GET_DETAILS,
  GET_MESSAGES,
  POST_MESSAGE,
  REACT_TO_MESSAGE,
} from '../constants/actions';

export const getRoomsAction = () => ({
  type: GET_ROOMS,
});

export const getRoomDetailsAction = id => ({
  type: GET_DETAILS,
  id,
});

export const getRoomMessagesAction = id => ({
  type: GET_MESSAGES,
  id,
});

export const postMessageAction = (id, name, message) => ({
  type: POST_MESSAGE,
  id,
  name,
  message,
});

export const postRoomAction = name => ({
  type: POST_ROOM,
  name,
});

export const reactToMessageAction = (roomId, messageId, name, reaction) => ({
  type: REACT_TO_MESSAGE,
  roomId,
  messageId,
  name,
  reaction,
});

export const getRooms = () => dispatch =>
  dispatch(getRoomsAction());

export const postRoom = name => dispatch =>
  dispatch(postRoomAction(name));

export const getRoomDetails = id => dispatch =>
  dispatch(getRoomDetailsAction(id));

export const getRoomMessages = id => dispatch =>
  dispatch(getRoomMessagesAction(id));

export const postMessage = (id, name, message) => dispatch =>
  dispatch(postMessageAction(id, name, message));

export const reactToMessage = (roomId, messageId, name, reaction) => dispatch =>
  dispatch(reactToMessageAction(roomId, messageId, name, reaction));
