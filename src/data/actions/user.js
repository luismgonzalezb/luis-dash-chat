import {
  LOG_IN,
  LOG_OUT,
} from '../constants/actions';

export const loginAction = userName => ({
  type: LOG_IN,
  userName,
});

export const login = userName => dispatch => dispatch(loginAction(userName));

export const logoutAction = userName => ({
  type: LOG_OUT,
  userName,
});

export const logout = userName => dispatch => dispatch(logoutAction(userName));
