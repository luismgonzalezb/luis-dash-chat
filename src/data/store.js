import { applyMiddleware, createStore, combineReducers } from 'redux';
import ReduxThunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import io from 'socket.io-client';
import createSocketIoMiddleware from 'redux-socket.io';

import user from './reducers/user';
import rooms from './reducers/rooms';
import config from './reducers/config';

export default function configureStore(initialState = {}) {
  const logger = createLogger();
  const reducer = combineReducers({
    user,
    rooms,
    config,
  });
  // The connection between the server and the client is done here.
  // Initially we open a Socket.io connection, then wrap it with a Redux middleware
  // that listens for the dispatch of actions that start with a pattern,
  // in this case 'IO/', This listener works both ways, it also catches
  // events emmited on the Socket.io Server that start with the same pattern.
  // This way, we simply provide action creators to send data to server and
  // Reducers to listen for the responses.
  // Also noted, a single constants file serves as the single source of truth
  // for both the Client and Server action types.
  const socket = io('http://localhost:8080');
  // use http://localhost:8080 for localhost development and https://luis-dash-chat.herokuapp.com on Heroku
  const socketIoMiddleware = createSocketIoMiddleware(socket, 'IO/');
  // Also added redux-thunk for async handler and redux-logger for testing/debugging
  const middleware = applyMiddleware(socketIoMiddleware, ReduxThunk, logger);
  const store = middleware(createStore)(reducer, initialState);
  return store;
}
