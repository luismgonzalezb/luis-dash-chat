import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { alert } from 'notie';
import FieldForm from './components/FieldForm';
import RoomList from './components/RoomList';
import RoomDisplay from './components/RoomDisplay';
import {
  login,
  logout,
} from './data/actions/user';
import {
  getRooms,
  postRoom,
  getRoomDetails,
  getRoomMessages,
  postMessage,
  reactToMessage,
} from './data/actions/rooms';
import logo from './logo-red.png';
import './DashChat.css';

class DashChat extends Component {
  constructor(props) {
    super(props);
    this.state = { selectedId: '-1' };
    this.selectRoom = this.selectRoom.bind(this);
    this.postMessage = this.postMessage.bind(this);
    this.reactToMessage = this.reactToMessage.bind(this);
    this.logout = this.logout.bind(this);
  }
  componentDidMount() {
    // Front Load Rooms for better user experience.
    this.props.getRooms();
    // This allows the user to be taken off the db
    // when the client is closed
    window.addEventListener('beforeunload', this.logout);
  }
  componentWillReceiveProps(nextProps) {
    const { config } = nextProps;
    if (config.error) {
      // Since we have a single error handler,
      // We only want to show messages that are new
      // Less than 2 milisecongds
      // (ideally should be zero, but I get a -1 sometimes)
      const diff = new Date(config.error.time) - new Date();
      if (diff > -2) {
        alert({
          type: 'error',
          text: config.error.msg,
        });
      }
    }
  }
  componentWillUnmount() {
    this.logout();
    window.removeEventListener('beforeunload', this.logout);
  }
  getSelectedRoom() {
    const { rooms } = this.props;
    const { selectedId } = this.state;
    return rooms.filter(room => room.id === selectedId)[0];
  }
  logout() {
    const { user } = this.props;
    if (user.name) {
      this.props.logout(user.name);
    }
  }
  selectRoom(selectedId) {
    // TODO: Avoid using DOM for this.
    const message = document.getElementById('message');
    if (message) {
      message.focus();
    }
    // This executes 2 server requests,
    // in a real world scenario most likely
    // details would come first as they'll
    // be less data
    this.props.getRoomDetails(selectedId);
    this.props.getRoomMessages(selectedId);
    this.setState({ selectedId });
  }
  postMessage(message) {
    const { user } = this.props;
    const { selectedId } = this.state;
    this.props.postMessage(selectedId, user.name, message);
  }
  reactToMessage(messageId, reaction) {
    const { user } = this.props;
    const { selectedId } = this.state;
    this.props.reactToMessage(selectedId, messageId, user.name, reaction);
  }
  render() {
    const { user, rooms } = this.props;
    const { selectedId } = this.state;
    const selectedRoom = this.getSelectedRoom();
    return (
      <div className="dashChat">
        {user.name ? (
          <div className="chat">
            <RoomList
              user={user}
              rooms={rooms}
              selectedId={selectedId}
              selectRoom={this.selectRoom}
              postRoom={this.props.postRoom}
            />
            <RoomDisplay
              user={user}
              room={selectedRoom}
              postMessage={this.postMessage}
              onReaction={this.reactToMessage}
            />
          </div>
        ) : (
          <div className="center-middle login">
            <img src={logo} alt="Door Dash Logo" />
            <FieldForm
              name="userName"
              className="form-col"
              placeholder="Enter a user name..."
              btnLabel="Join the DoorDash Chat!"
              onSubmit={this.props.login}
            />
          </div>
        )}
      </div>
    );
  }
}

DashChat.propTypes = {
  user: PropTypes.object.isRequired,
  rooms: PropTypes.array.isRequired,
  config: PropTypes.object.isRequired,
  login: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  getRooms: PropTypes.func.isRequired,
  postRoom: PropTypes.func.isRequired,
  getRoomDetails: PropTypes.func.isRequired,
  getRoomMessages: PropTypes.func.isRequired,
  postMessage: PropTypes.func.isRequired,
  reactToMessage: PropTypes.func.isRequired,
};

const mapStateToProps = ({ user, rooms, config }) => ({
  user,
  rooms,
  config,
});

export default connect((mapStateToProps), {
  login,
  logout,
  getRooms,
  postRoom,
  getRoomDetails,
  getRoomMessages,
  postMessage,
  reactToMessage,
})(DashChat);
