const express = require('express');

const app = express();
const bodyParser = require('body-parser');
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const api = require('./api');
const database = require('./api/database');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const port = process.env.PORT || 8080;

const router = express.Router();

// Unsafely enable cors
router.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

// logging middleware
router.use((req, res, next) => {
  console.log('\nReceived:', { url: req.originalUrl, body: req.body, query: req.query });
  next();
});

// Socket IO Implementation
server.listen(port, () => {
  console.log('Server listening at port %d', port);
});

// Start The Server to Listen for connections
io.on('connection', (socket) => {
  // Received a Socket IO Client connection
  api.actions(socket, database);
});

// API Routes
router.get('/rooms', (req, res) => {
  const rooms = database.getRooms();
  console.log('Response:', rooms);
  res.json(rooms);
});

router.get('/rooms/:roomId', (req, res) => {
  const room = database.getRoom(req.params.roomId);
  res.json(room);
});

router.route('/rooms/:roomId/messages')
  .get((req, res) => {
    const messages = database.getMessages(req.params.roomId);
    console.log('Response:', messages);
    res.json(messages);
  })
  .post((req, res) => {
    const room = database.postMessage(req.params.roomId, req.body.name, req.body.message);
    console.log('Response:', room);
    res.json(room);
  });

app.use('/api', router);
app.use(express.static('build'))

console.log(`API running at localhost:${port}/api`);
